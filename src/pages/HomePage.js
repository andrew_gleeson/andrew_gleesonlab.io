import React from 'react';
import {Avatar, Divider} from 'antd';
import Me from '../img/me.jpg'
import MediaQuery from 'react-responsive';

class HomePage extends React.Component {
    render = () => {
        document.title = 'Andrew Gleeson';
        return (
            <div style={{textAlign: 'center'}}>
                <Avatar size={256} src={Me}/><br/>
                <MediaQuery query="(min-device-width: 711px)">
                    <Divider style={{fontSize: '48px', marginTop: '30px', fontFamily: '\'Montserrat\''}}>
                        Andrew Gleeson
                    </Divider>
                </MediaQuery>
                <MediaQuery query="(max-device-width: 711px)">
                    <div style={{fontSize: '32px', margin: '30px 0 20px', width: '100%'}}>
                        Andrew Gleeson
                    </div>
                </MediaQuery>
                <div style={{maxWidth: '500px', textAlign: 'left', display: 'inline-block', marginTop: '0'}}>
                    I'm a recent Physics and Computer Science graduate from UC Berkeley with a focus in Quantum
                    Computing.
                    I have 4 years experience as a full-stack software engineer at tech startups
                    in the SF Bay Area, and am currently working
                    at <a href="http://www.rigetti.com/about" target="_blank" rel="noopener noreferrer">Rigetti
                    Computing</a> to
                    build the world's first universal quantum computer.
                </div>
            </div>
        );
    }
}

export default HomePage;