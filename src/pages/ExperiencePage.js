import React from 'react';
import {Avatar, Divider, List} from "antd";

import rigetti from '../img/rigetti.png';
import wikia from '../img/wikia.jpeg';
import cal from '../img/cal.png';
import academia from '../resources/academia.json'
import industry from '../resources/industry.json'

const avatars = {
    'cal': cal,
    'wikia': wikia,
    'rigetti': rigetti
};

class ExperiencePage extends React.Component {
    render = () => {
        document.title = 'Andrew Gleeson | Experience';
        return (
            <div>
                <Divider orientation="left" style={{marginTop: 0}}>Academia</Divider>
                <List
                    itemLayout="vertical"
                    size="large"
                    dataSource={academia}
                    renderItem={item => (
                        <List.Item
                            key={item.title + item.institution}
                        >
                            <List.Item.Meta
                                avatar={<Avatar src={avatars[item.avatar]}/>}
                                title={<span>{item.title} at {item.institution}</span>}
                                description={(
                                    <div>
                                        <div>{item.dates.start} to {item.dates.end}</div>
                                        <div>GPA: {item.gpa}</div>
                                    </div>
                                )}
                            />
                            <div style={{marginLeft: '48px'}}>
                                {item.description}
                                <h4 style={{marginTop: '5px'}}>Selected Upper-Division Courses</h4>
                                {item.classes.map(cls => <div>{cls.id} | {cls.name}</div>)}
                            </div>
                        </List.Item>
                    )}
                />
                <Divider orientation="left" style={{marginTop: 0}}>Industry</Divider>
                <List
                    itemLayout="vertical"
                    size="large"
                    dataSource={industry}
                    renderItem={job => (
                        <List.Item
                            key={job.title + job.company}
                        >
                            <List.Item.Meta
                                avatar={<Avatar src={avatars[job.avatar]} shape="square"/>}
                                title={<span>{job.title} at {job.company}</span>}
                                description={`${job.dates.start} to ${job.dates.end}`}
                            />
                            <div style={{marginLeft: '48px'}}>{job.description}</div>
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}

export default ExperiencePage;