import React from 'react';
import {Button, Drawer, Menu} from 'antd';
import {Redirect, withRouter} from "react-router-dom";

class MobileMenu extends React.Component {
    state = {
        visible: false,
        path: undefined
    };

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    onClick = (e) => {
        this.setState({
            path: e.key,
            visible: false
        }, () => this.setState({path: undefined}));
    };

    render = () => {
        if (this.state.path) return <Redirect to={this.state.path}/>;
        return (
            <div>
                <Menu
                    mode="horizontal"
                    style={{lineHeight: '45px'}}
                    selectable={false}
                >
                    <Menu.Item key="0"
                               disabled={true}
                               style={{color: '#656565', cursor: 'default', fontSize: '1.5em'}}
                    >
                        Andrew Gleeson
                    </Menu.Item>
                    <Menu.Item key="button" style={{float: 'right'}}>
                        <Button type="primary"
                                onClick={this.showDrawer}
                                style={{backgroundColor: 'darkgreen', borderColor: 'darkgreen'}}>
                            Menu
                        </Button>
                    </Menu.Item>
                </Menu>

                <Drawer
                    title="Navigation"
                    placement="right"
                    closable={false}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    style={{padding: 0}}
                >
                    <Menu
                        onClick={this.onClick}
                        defaultSelectedKeys={[this.props.location.pathname]}
                        mode="inline"
                    >
                        {this.props.navigation.filter(route => route.component).map(route => (
                            <Menu.Item key={route.path}>{route.name}</Menu.Item>
                        ))}
                        {this.props.navigation.filter(route => !route.component).map(route => (
                            <Menu.Item key={route.path}>
                                <a href={route.path} onClick={e => e.stopPropagation()} target="_blank" rel="noopener noreferrer">{route.name}</a>
                            </Menu.Item>
                        ))}
                    </Menu>
                </Drawer>
            </div>
        );
    }
}

export default withRouter(MobileMenu);


