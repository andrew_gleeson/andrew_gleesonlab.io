import {Link, withRouter} from "react-router-dom";
import {Menu} from "antd";
import React from "react";

class NavMenu extends React.Component {
    render = () => {
        return (
            <Menu
                mode="horizontal"
                style={{lineHeight: '45px', marginLeft: '50px'}}
                defaultSelectedKeys={[this.props.location.pathname]}

            >
                <Menu.Item key="0"
                           disabled={true}
                           style={{color: '#656565', cursor: 'default', fontSize: '1.5em'}}
                >
                    Andrew Gleeson
                </Menu.Item>
                {this.props.navigation.filter(route => route.component).map(route => <Menu.Item key={route.path}><Link
                    to={route.path}>{route.name}</Link></Menu.Item>)}
                {this.props.navigation.filter(route => !route.component).map(route => <Menu.Item key={route.path}>
                    <a href={route.path} target="_blank" rel="noopener noreferrer">{route.name}</a>
                </Menu.Item>)}

            </Menu>
        );
    }
}

export default withRouter(NavMenu);