import React from 'react';
import {Divider, Icon, List, Row, Col, Modal} from "antd";
import Latex from 'react-latex';

import projects from '../resources/projects.json';
import chsh from '../img/chsh.png';
import tensor from '../img/tensor.png';
import rbm from '../img/rbm.png';
import learntocode from '../img/learntocode.png';
import error from '../img/error.png';
import agsp from '../img/agsp.png';
import halfwave from '../img/halfwave.png';
import recursive from '../img/recursive.png';
import results from '../img/results.png';
import sparknotes_before from '../img/sparknotes_before.png';
import sparknotes_after from '../img/sparknotes_after.png';
import chsh_file from '../resources/CHSH_Gleeson.pdf';
import qnn from '../resources/nn_quantum_states.pdf';
import spinchain from '../resources/Spin_Chains_Gleeson.pdf';


const images = {
    'chsh': chsh,
    'tensor': tensor,
    'rbm': rbm,
    'learntocode': learntocode,
    'error': error,
    'agsp': agsp,
    'halfwave': halfwave,
    'recursive': recursive,
    'results': results,
    'sparknotes_before': sparknotes_before,
    'sparknotes_after': sparknotes_after
};

const files = {
    'chsh': chsh_file,
    'qnn': qnn,
    'spinchain': spinchain
};

class ImageList extends React.Component {

    state = {
        modalVisible: undefined
    };

    onClick = (img) => {
        this.setState({
            modalVisible: img
        });
    };

    handleClose = () => {
        this.setState({
            modalVisible: undefined
        })
    };

    render = () => {

        const item = this.props.item;

        if (!item.img || item.img.length === 0) return null;

        return (
            <List
                dataSource={item.img}
                renderItem={img => (
                    <List.Item>
                        <img onClick={e => this.onClick(img)} src={images[img]} style={{width: '100%', cursor: 'pointer'}}
                             alt={item.title}/>
                        <Modal
                            centered={true}
                            visible={this.state.modalVisible === img}
                            onOk={this.handleClose}
                            onCancel={this.handleClose}
                            footer={null}
                            bodyStyle={{padding: 0, textAlign: 'center'}}
                            closable={false}
                        >
                            <img src={images[img]} style={{maxWidth: '100%', maxHeight: 500}} alt={item.title}/>
                        </Modal>
                    </List.Item>
                )}
            />
        );
    }
}

class ProjectsPage extends React.Component {
    render = () => {
        document.title = 'Andrew Gleeson | Projects';
        return (
            <div>
                <Divider orientation="left" style={{marginTop: 0}}>Projects</Divider>
                <List
                    itemLayout="vertical"
                    size="large"
                    dataSource={projects}
                    renderItem={item => (
                        <List.Item
                            key={item.title}
                        >
                            <h3>{item.title}</h3>
                            <Row gutter={16}>
                                <Col span={item.img && item.img.length > 0 ? 14 : 24}>
                                    <div style={{color: 'grey', marginBottom: '10px'}}>{item.date}</div>
                                    <Latex>{item.description}</Latex>
                                    <div style={{margin: '10px 10px 0'}}>
                                        {item.url ? (
                                                <div>
                                                    <a href={item.url}>
                                                    <span style={{color: 'grey', fontSize: '10px'}}>
                                                        <Icon type="link" style={{marginRight: '7px'}}/>
                                                        {item.urlname}
                                                    </span>
                                                    </a>
                                                </div>
                                            )
                                            : null}
                                        {item.file && item.fname ? (
                                                <div>
                                                    <a href={files[item.file]}>
                                                <span style={{color: 'grey', fontSize: '10px'}}>
                                                    <Icon type="file" style={{marginRight: '7px'}}/>
                                                    {item.fname}
                                                </span>
                                                    </a>
                                                </div>
                                            )
                                            : null}
                                    </div>
                                </Col>
                                <Col span={item.img && item.img.length > 0 ? 10 : 0}>
                                    <ImageList item={item}/>
                                </Col>
                            </Row>
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}

export default ProjectsPage;