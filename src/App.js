import React, {Component} from 'react';
import './App.css';
import {Icon, Layout} from 'antd';
import {BrowserRouter, Route} from "react-router-dom";
import ExperiencePage from "./pages/ExperiencePage";
import HomePage from "./pages/HomePage";
import ProjectsPage from "./pages/ProjectsPage";
import MediaQuery from "react-responsive";
import MobileMenu from "./pages/components/MobileMenu";
import NavMenu from "./pages/components/NavMenu";

const navigation = [
    {
        name: 'Home',
        path: '/',
        component: HomePage
    },
    {
        name: 'Experience',
        path: '/experience',
        component: ExperiencePage
    },
    {
        name: 'Projects',
        path: '/projects',
        component: ProjectsPage
    },
    {
        name: 'Resume',
        path: '/resume.pdf',
        component: null
    }
];

class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <Layout>
                        <Layout.Header style={{backgroundColor: 'white', height: '45px', padding: 0}}>
                            <MediaQuery query="(min-device-width: 711px)">
                                <NavMenu navigation={navigation}/>
                            </MediaQuery>
                            <MediaQuery query="(max-device-width: 711px)">
                                <MobileMenu navigation={navigation}/>
                            </MediaQuery>
                        </Layout.Header>
                        <Layout.Content style={{
                            padding: '20px 20px 0',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <div style={{background: 'white', padding: 24, minHeight: 280, width: 600}}>
                                {navigation.filter(route => route.component)
                                    .map(route => <Route key={route.path} path={route.path} exact
                                                         component={route.component}/>)}
                            </div>
                        </Layout.Content>
                        <Layout.Footer style={{textAlign: 'center'}}>
                            <Icon type="copyright" style={{marginRight: '5px'}}/> Andrew Gleeson 2018
                        </Layout.Footer>
                    </Layout>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
